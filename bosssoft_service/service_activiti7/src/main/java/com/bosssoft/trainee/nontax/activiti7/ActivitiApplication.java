package com.bosssoft.trainee.nontax.activiti7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * 工作流启动器
 *
 * @author wzy
 * @since 2021-04-01 14:45
 */
@SpringBootApplication
@ComponentScan("com.bosssoft")
@EnableDiscoveryClient
@EnableFeignClients
public class ActivitiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ActivitiApplication.class, args);
    }
}
