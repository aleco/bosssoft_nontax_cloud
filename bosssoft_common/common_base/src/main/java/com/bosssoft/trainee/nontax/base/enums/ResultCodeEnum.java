package com.bosssoft.trainee.nontax.base.enums;

import lombok.Getter;

/**
 * 统一返回结果状态信息类
 *
 * @author wzy
 * @since 2021-03-31 10:14
 */
@Getter
public enum ResultCodeEnum {

    /**
     * 成功
     */
    SUCCESS(200, "成功"),
    /**
     * 失败
     */
    ERROR(500, "失败"),
    /**
     * 请先登录
     */
    UNAUTHORIZED(401, "请先登录"),

    /**
     * 用户不存在
     */
    USER_NOT_FOUND(402, "用户不存在"),
    /**
     * 退出成功！
     */
    LOGOUT(200, "退出成功！"),
    /**
     * 权限不足
     */
    ACCESS_DENIED(403, "权限不足"),
    /**
     * 请求不存在
     */
    REQUEST_NOT_FOUND(404, "请求不存在"),
    /**
     * 请求方式不支持！
     */
    HTTP_BAD_METHOD(405, "请求方式不支持！"),
    /**
     * 请求异常！
     */
    BAD_REQUEST(400, "请求异常！"),

    /**
     * 参数不匹配！
     */
    PARAM_NOT_MATCH(400, "参数不匹配！"),

    /**
     * 参数不能为空！
     */
    PARAM_NOT_NULL(400, "参数不能为空！"),
    /**
     * 当前用户已被锁定，请联系管理员解锁！
     */
    USER_DISABLED(403, "当前用户已被锁定，请联系管理员解锁！"),

    /**
     * 用户名或密码错误！
     */
    USERNAME_PASSWORD_ERROR(5001, "用户名或密码错误！"),

    /**
     * 禁止访问
     */
    FORBIDDEN(5001, "禁止访问"),

    /**
     * token 已过期，请重新登录！
     */
    TOKEN_EXPIRED(5002, "token 已过期，请重新登录！"),

    /**
     * token 解析失败，请尝试重新登录！
     */
    TOKEN_PARSE_ERROR(5002, "token 解析失败，请尝试重新登录！"),

    /**
     * 当前用户已在别处登录，请尝试更改密码或重新登录！
     */
    TOKEN_OUT_OF_CTRL(5003, "用户信息发生变化，请重新登录！"),
    ;

    private final Integer code;
    private final String message;

    ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
